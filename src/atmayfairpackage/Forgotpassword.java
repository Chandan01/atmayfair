package atmayfairpackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Forgotpassword  {
	  public  WebDriver driver;
	  public  String baseUrl;
	  
	 public  StringBuffer verificationErrors = new StringBuffer();
    
	  
	  @BeforeTest
	  public void setUp() throws Exception
	  {
	    driver = new FirefoxDriver();
	    baseUrl = "http://staging.atmayfair.com/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }
		  
	    @Test
		  public void testsinup() throws InterruptedException  {
	      driver.get(baseUrl);
	      
	      // verify current url 
	     String  currentURL = driver.getCurrentUrl();
	     System.out.println(currentURL);
	     
	     // current  title for home page 
	     
	     String title = driver.findElement(By.xpath("//*[@id='logo']/a/img[1]")).getText();
	     
		    try {
		      AssertJUnit.assertEquals("Atmayfair   ", title);
		      System.out.println(title);
		    } catch (Error e) {
		      verificationErrors.append(e.toString());
		      
		    }
		   //Click on sign in button 
		   WebElement  sign_button=driver.findElement(By.xpath("//html/body/div[5]/div[2]/div[1]/div/header/div[1]/div/div/div[2]/ul/li[2]/a"));
		    
		   sign_button.click();	
		   
		   
		   //click on log in with email 
		   
		   WebElement  loginwithink=driver.findElement(By.xpath("//a[text()='Login With Email']"));
		   
		   loginwithink.click();
		   
		   //Click on forget password link 
		   
		   driver.findElement(By.linkText("Forgot Your Password?")).click();
		   
		   driver.findElement(By.id("forgot-email")).sendKeys("vinit.v@innovify.in");
		   
		   
		   //reset password button 
		   
		   driver.findElement(By.name("fogot-popup-btn")).click();
		   
		    
		     
	   } 	
	    @AfterTest
	    public void Close()
	    {
	    	driver.close();
	    	
	    	
	    }
	    }
	    	
	    	
	    
		 