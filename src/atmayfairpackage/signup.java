package atmayfairpackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class signup  {
	  public  WebDriver driver;
	  public  String baseUrl;
	  
	 public  StringBuffer verificationErrors = new StringBuffer();
    
	  
	  @BeforeTest
	  public void setUp() throws Exception
	  {
	    driver = new FirefoxDriver();
	    baseUrl = "http://staging.atmayfair.com/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }
		  
	    @Test
		  public void testsinup() throws InterruptedException  {
	      driver.get(baseUrl);
	      
	      // verify current url 
	     String  currentURL = driver.getCurrentUrl();
	     System.out.println(currentURL);
	     
	     // current  title for home page 
	     
	     String title = driver.findElement(By.xpath("//*[@id='logo']/a/img[1]")).getText();
	     
		    try {
		      AssertJUnit.assertEquals("Atmayfair   ", title);
		      System.out.println(title);
		    } catch (Error e) {
		      verificationErrors.append(e.toString());
		      
		    }
		   //Click on sign in button 
		   WebElement  sign_button=driver.findElement(By.xpath("//html/body/div[5]/div[2]/div[1]/div/header/div[1]/div/div/div[2]/ul/li[2]/a"));
		    
		   sign_button.click();	
		   
		   //click on Sign Up  link
		   
		   WebElement  signlink=driver.findElement(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div[2]/div[2]/div[3]/div[1]/a"));
		   
		   signlink.click();
		   
		 //click on Sign Up With Email link
		   
		   WebElement  signlinkemail =driver.findElement(By.linkText("Sign Up With Email"));
		   
		   signlinkemail.click();
		   
		 // Enter email id and password 
		
		   WebElement email_textbox=driver.findElement(By.id("register-email"));
		   
		   email_textbox.sendKeys("vinit.v@innovify.in");
		   
		   
		   WebElement  password_textbox=driver.findElement(By.id("register-password"));
		  
		  password_textbox.sendKeys("amf@1234");
		  
		  
		  // Click on sign up window 
		  
		   WebElement  signup_button =driver.findElement(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div[2]/div[4]/form/div[4]/button"));
		    signup_button.click();
		    
		    
		    //Verify user is navigating to  profile page after clicking on sign up button
		    
	
		    
		    String  Actual_title = driver.findElement(By.xpath("//*[@id='form-validate-changename']/div[1]/div[1]/h1")).getText();
		    
		    String expected_title="My Profile Details";
		    
		    Assert.assertEquals(Actual_title, expected_title);
		    
		    System.out.println(Actual_title);
		   //verify sign up with already registered  email id
		    
		    String  actual_message=driver.findElement(By.xpath("//html/body/div[2]/div/div/div/div[2]/div/div[2]/div[4]/form/div[4]/button")).getText();
		    String  expected_messsage = "This email ID is already registered with AtMayfair.";
		 
		    //Assert.assertEquals(actual_message, expected_messsage);
		    
		    try {
			      AssertJUnit.assertEquals(actual_message, expected_messsage);
			    } catch (Error e) {
			      verificationErrors.append(e.toString());
			    }	    
		    
		     
	   } 	
	    	
	    	
	    }
		 
		  
	  
	  
	  
		
	


